import { Component, OnInit } from '@angular/core';
import { School } from '../school';
import { SchoolService } from '../school.service';


@Component({
  selector: 'app-add-school',
  templateUrl: './add-school.component.html',
  styleUrls: ['./add-school.component.css']
})
export class AddSchoolComponent implements OnInit {

  school : School = {
    schoolId: 0,
    schoolName : "",
    schoolAddress: ""
  }
  
  constructor(private schoolService: SchoolService) { }

  ngOnInit(): void {
   this.onSubmit();
  }

  onSubmit = () => {
    this.schoolService.addData(this.school).subscribe(
      error => {
        console.error("errorMsg",error);
      }
    );
  };


}
