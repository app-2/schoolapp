import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchoolsComponent} from './schools/schools.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SchoolDetailComponent } from './school-detail/school-detail.component';
import { AddSchoolComponent } from './add-school/add-school.component';
import { DeleteSchoolComponent } from './delete-school/delete-school.component';
import { UpdateComponent } from './update/update.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'schools', component: SchoolsComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'addschool', component: AddSchoolComponent },
  { path: 'deleteschool', component: DeleteSchoolComponent },
  { path: 'updateschool', component: UpdateComponent },

  { path: 'detail/:id', component: SchoolDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }