import { Component, OnInit } from '@angular/core';
import { SchoolService } from '../school.service';
import { School } from '../school';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private schoolService : SchoolService) { }
  schools: School[]= [];

  ngOnInit(): void {
    this.getSchools();
  }
  getSchools(): void {
    this.schoolService.getSchools()
      .subscribe(schools => this.schools = schools.slice(1, 5));
  }
}
