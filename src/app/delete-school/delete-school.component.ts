import { Component, OnInit } from '@angular/core';
import { School } from '../school';
import { SchoolService } from '../school.service';

@Component({
  selector: 'app-delete-school',
  templateUrl: './delete-school.component.html',
  styleUrls: ['./delete-school.component.css']
})
export class DeleteSchoolComponent implements OnInit {

  id = 0;
  constructor(private schoolService:SchoolService) { }

  ngOnInit(): void {
    this.onSubmit();
  }

  onSubmit = () => {
    this.schoolService.deleteData(this.id).subscribe(
      error => {
        console.error("errorMsg",error);
      }
    );
  };

}
