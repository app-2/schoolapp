import { School } from './school';

export const SCHOOLS: School[] = [
  { schoolId: 11, schoolName: 'Dr Nice',   schoolAddress: 'Karanchi' },
  { schoolId: 12, schoolName: 'Narco',     schoolAddress: 'Ranchi' },
  { schoolId: 13, schoolName: 'Bombasto',  schoolAddress: 'London' },
  { schoolId: 14, schoolName: 'Celeritas', schoolAddress: 'Punjab' },
  { schoolId: 15, schoolName: 'Magneta',   schoolAddress: 'Delhi' },
  { schoolId: 16, schoolName: 'RubberMan', schoolAddress: 'Goa' },
  { schoolId: 17, schoolName: 'Dynama',    schoolAddress: 'Mumbai' },
  { schoolId: 18, schoolName: 'Dr IQ',     schoolAddress: 'Jaipur' },
  { schoolId: 19, schoolName: 'Magma',     schoolAddress: 'Haryana' },
  { schoolId: 20, schoolName: 'Tornado',   schoolAddress: 'Patna' }
];