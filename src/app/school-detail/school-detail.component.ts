import { Component, OnInit, Input } from '@angular/core';
import { School} from '../school';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { SchoolService } from '../school.service';


@Component({
  selector: 'app-school-detail',
  templateUrl: './school-detail.component.html',
  styleUrls: ['./school-detail.component.css']
})
export class SchoolDetailComponent implements OnInit {

  constructor( private route: ActivatedRoute,
    private schoolService: SchoolService,
    private location: Location) { }
   school: School | undefined;

  ngOnInit(): void {
    this.getSchool();
  }
  
  getSchool(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id')!, 10);
    this.schoolService.getSchool(id)
      .subscribe(school => this.school = school);
  }
  goBack(): void {
    this.location.back();
  }
}
