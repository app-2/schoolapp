import { Injectable } from '@angular/core';
import { School } from './school';
import { SCHOOLS } from './mock-schools';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SchoolService {

  private schoolsUrl = 'https://localhost:44388/api/SchoolManagement/getschool';

  constructor( private http: HttpClient,
    private messageService: MessageService){ }


    getSchools(): Observable<School[]> {
      return this.http.get<School[]>(this.schoolsUrl)
      .pipe(
        tap(_ => this.log('fetched schools')),
        catchError(this.handleError<School[]>('getSchools', []))
      );
    }

    getSchool(id: number): Observable<School> {
      const url = `${this.schoolsUrl}/${id}`;
      return this.http.get<School>(url).pipe(
        tap(_ => this.log(`fetched school id=${id}`)),
        catchError(this.handleError<School>(`getSchool id=${id}`))
      );
    }

    addData = (school:School)=>{
      console.log(school)
      return this.http.post(`${environment.ApiPostUrl}`,school);
    }

    deleteData(id:number):Observable<any>{
      return this.http.delete(`${environment.ApiDeleteUrl}?id=${id}`);
    }

    updateData = (school:School) =>{
      console.log(school)
      return this.http.put(`${environment.ApiPutUrl}`,school);
    }
    
  private log(message: string) {
    this.messageService.add(`SchoolService: ${message}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
