export interface School {
    schoolId: number;
    schoolName: string;
    schoolAddress: string;
  }