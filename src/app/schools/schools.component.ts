import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { School } from '../school';
import { SchoolService } from '../school.service';

@Component({
  selector: 'app-schools',
  templateUrl: './schools.component.html',
  styleUrls: ['./schools.component.css']
})
export class SchoolsComponent implements OnInit {

  constructor(private schoolService: SchoolService) { }
  schools: School[] = [];
 
  ngOnInit(): void {
    this.getSchools();
  }
  getSchools(): void {
    this.schoolService.getSchools()
      .subscribe(schools => this.schools = schools);
  }
}
