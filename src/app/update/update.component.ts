import { Component, OnInit } from '@angular/core';
import { School } from '../school';
import { SchoolService } from '../school.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  school : School = {
    schoolId: 0,
    schoolName : "",
    schoolAddress: ""
  }
  constructor(private schoolService : SchoolService) { }

  ngOnInit(): void {
    this.onSubmit();
  }

  onSubmit = () => {
    this.schoolService.updateData(this.school).subscribe(
      error => {
        console.error("errorMsg",error);
      }
    );
  };
}
